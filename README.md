# LSP Server for WebHare

Experimental [Language Server Protocol](https://microsoft.github.io/language-server-protocol/specifications/specification-current/) implementation for [WebHare](https://gitlab.com/webhare/platform).

This language server can be used by a language client to provide syntax checking and other language features for supported WebHare files. It connects to a WebHare server through the [`dev` WebHare module](https://gitlab.com/webhare/dev). See that repository for more information.
