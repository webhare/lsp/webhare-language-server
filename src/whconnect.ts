/* The connection with the WebHare server */

import * as fs from "fs";
import { resolve } from "path";
import { URL } from "url";
import { DocumentUri } from 'vscode-languageserver';
import * as WebSocket from "ws";

import { connection, connectionConfig } from "./connection";
import { ConfigResponse } from "./protocol";
import { handleServerRequest } from "./service";


export interface ConnectInfo {
	editorservice: string;
	accesstoken: string;
	translateinfo: string;
}

interface QueuedMessage {
	resolve: (result: any) => void;
	reject: (reason: any) => void;
}


// A cache of WebHare connections (maps connect info directory paths to connections)
const webHareConnections: Map<string, Promise<WebHareConnection>> = new Map();
// A cache of document connections (maps document resources to connections)
const documentConnections: Map<string, Promise<WebHareConnection | null>> = new Map();

const LSP_JSON_VERSION = "2.0";
const CONFIGURATION_SETTINGS = [ "documentation-diagnostics", "debug-loglevel" ];


// The WebHare connection
export class WebHareConnection {
	private address: URL;
	private token: string;
	private translateInfo: string;
	private basePath: string;
	private baseProto: string;
	private basePathWithoutProto: string;

	private socket?: WebSocket;
	private msgId: number;
	private queue: Map<number, QueuedMessage>;

	private config: ConfigResponse | null = null;

	constructor(settings: ConnectInfo, basePath: string)
	{
		this.address = new URL(settings.editorservice);
		this.address.pathname = "/.dev/lsp.whsock";
		this.token = settings.accesstoken;
		this.translateInfo = settings.translateinfo;
		this.msgId = 0;
		this.queue = new Map();

		this.basePath = basePath;
		// resolve doesn't handle protocols very well (e.g. "file:///tmp/" gets turned into "/file:/tmp/"), so we'll keep the
		// protocol separate from the base path to resolve relative paths into absolute paths
		this.baseProto = "";
		this.basePathWithoutProto = this.basePath;
		const idx = this.basePathWithoutProto.indexOf("://");
		if (idx > 0) {
			this.baseProto = this.basePathWithoutProto.substr(0, idx + 3);
			this.basePathWithoutProto = this.basePathWithoutProto.substr(idx + 3);
			// Make sure the base path start with a slash, otherwise resolve doesn't work
			if (!this.basePathWithoutProto.startsWith("/")) {
				this.baseProto = this.baseProto.substr(this.baseProto.length - 1);
				this.basePathWithoutProto = "/" + this.basePathWithoutProto;
			}
		}
	}

	// Connect to WebHare, returns a promise that resolves when fully connected
	connect(): Promise<WebHareConnection> {
		return new Promise(async (resolve, reject) => {
			// Get current workspace configuration, keep only the settings we're interested in
			const config = await connection.workspace.getConfiguration("webhare");
			const webhare = getWebHareConfig(config);

			// Create a WebSocket connection
			connection.console.log(`Connecting to ${this.address}`);
			this.socket = new WebSocket(this.address.toString());
			this.socket.on("message", data => this.onSocketMessage(data));
			this.socket.on("error", err => this.onSocketError(err));
			this.socket.on("close", () => this.onSocketClose());
			this.msgId = 0;
			this.queue.clear();

			// Send a 'connect' message with our configuration when connected
			this.socket.on("open", () => {
				this.sendRequest("connect", this.token, this.translateInfo, connectionConfig, webhare).then(() => {
					connection.console.log("Connected!");
					resolve(this);
				}).catch(reject);
			});
		});
	}

	// Send a request, returns a promise that resolves with the response to the request
	async sendRequest(method: string, ...params: any[]): Promise<any> {
		if (!this.socket) {
			await this.connect();
		}
		return await new Promise((resolve, reject): void => {
			const message = {
				jsonrpc: LSP_JSON_VERSION,
				id: ++this.msgId,
				method,
				params
			};
			connection.console.log(`Sending '${method}' request with id ${message.id}`);
			this.queue.set(message.id, { resolve, reject });
			if (this.socket) {
				this.socket.send(JSON.stringify(message), err => err && reject(err));
			} else {
				reject(new Error("Not connected"));
			}
		});
	}

	// Send a notification (a request without a response), returns a promise that resolves when the notification is sent
	async sendNotification(method: string, ...params: any[]): Promise<void> {
		if (!this.socket) {
			await this.connect();
		}
		await new Promise<void>((resolve, reject): void => {
			const message = {
				jsonrpc: LSP_JSON_VERSION,
				method,
				params
			};
			connection.console.log(`Sending '${method}' notification`);
			if (this.socket) {
				this.socket.send(JSON.stringify(message), err => err ? reject(err) : resolve());
			} else {
				reject(new Error("Not connected"));
			}
		});
	}

	// Send a result in response to a server request back to the server
	private async sendResult(id: number, result: any): Promise<void> {
		if (!this.socket) {
			await this.connect();
		}
		await new Promise<void>((resolve, reject): void => {
			const message = {
				jsonrpc: LSP_JSON_VERSION,
				id,
				result
			};
			connection.console.log(`Sending result for id ${id}`);
			if (this.socket) {
				this.socket.send(JSON.stringify(message), err => err ? reject(err) : resolve());
			} else {
				reject(new Error("Not connected"));
			}
		});
	}

	// Convert an absolute uri to a relative path as expected by the WebHare LSP implementation
	getRelativePath(uri: string) {
		if (uri.startsWith(this.basePath)) {
			return uri.substring(this.basePath.length);
		}
		return "";
	}

	// Convert a relative path as returned by the WebHare LSP implementation to an absolute uri
	getAbsolutePath(path: string) {
		return this.baseProto + resolve(this.basePathWithoutProto, path);
	}

	// Convert the uri of a TextDocument(Item, Identifier) to a relative path as expected by the WebHare LSP implementation
	convertTextDocumentUri<Type extends { uri: DocumentUri }>(doc: Type): Type {
		return { ...doc, uri: this.getRelativePath(doc.uri) };
	}

	// Get WebHare configuration
	async getConfig(): Promise<ConfigResponse> {
		if (!this.config) {
			this.config = await this.sendRequest("configRequest") as ConfigResponse;
		}
		return this.config;
	}

	private async onSocketMessage(data: WebSocket.Data) {
		try {
			const message = JSON.parse(<string>data);
			if ("result" in message) {
				connection.console.log(`Received result for id ${message.id}`);
				const queued = this.queue.get(message.id);
				if (queued) {
					this.queue.delete(message.id);
					queued.resolve(message.result);
				}
			} else if ("error" in message) {
				connection.console.log(`Received error for id ${message.id}`);
				const queued = this.queue.get(message.id);
				if (queued) {
					this.queue.delete(message.id);
					queued.reject(new Error(message.error.string));
				}
			} else {
				connection.console.log(`Received '${message.method}' ${message.id > 0 ? `message with id ${message.id}` : "notification"}`);
				const response = await handleServerRequest(this, message.method, message.params);
				// If the message has an id, return the response
				if (message.id > 0) {
					this.sendResult(message.id, response);
				}
			}
		} catch(err: any) {
			connection.console.error(err);
		}
	}

	private onSocketError(err: any) {
		connection.console.error(err);
	}

	private onSocketClose() {
		connection.console.warn("Socket closed");
		this.socket = void 0;
		// Remove all cached references to this connection
		webHareConnections.delete(this.basePath);
		for (const resource of documentConnections.keys()) {
			if (resource.startsWith(this.basePath))
			documentConnections.delete(resource);
		}
	}
}


function getConnection(resource: string): Promise<WebHareConnection|null> {
	// First, check if we have a connection with a base path that contains the resource
	for (const conn of webHareConnections.entries()) {
		if (resource.startsWith(conn[0])) {
			connection.console.log("Reusing existing connection for "+resource);
			return conn[1];
		}
	}
	// Find the connect info for the resource
	const url = new URL(resource);
	const pathParts = url.pathname.split("/");
	while (pathParts.length > 0) {
		pathParts.pop();
		// Check if a connect info file exists in this directory
		url.pathname = pathParts.join("/") + "/.wh.dev.connectinfo";

		if (fs.existsSync(url)) {
			let data;
			try {
				data = fs.readFileSync(url, { encoding: "utf-8", flag: "r" });
			} catch(err) {
				return Promise.reject(err);
			}
			// Read the connection info
			let settings: ConnectInfo;
			try {
				settings = JSON.parse(data);
			} catch(err) {
				return Promise.reject(err);
			}

			// Open and store the connection for this base path
			url.pathname = pathParts.join("/") + "/";
			const basePath = url.toString();
			connection.console.log("Creating new connection for "+resource);
			const conn: WebHareConnection = new WebHareConnection(settings, basePath);
			const result = conn.connect();
			webHareConnections.set(basePath, result);
			return result;
		}
	}
	return Promise.resolve(null);
}

function getWebHareConfig(config: { [key: string]: any }) {
	const webhare: { [key: string]: any } = {};
	let any_keys = false;
	connection.console.log("getWebHareConfig: "+JSON.stringify(config));
	if (config) {
		for (const key of CONFIGURATION_SETTINGS) {
			if (key in config) {
				any_keys = true;
				webhare[key] = config[key];
			} else {
				// Simple dashes-to-camelCase converter
				const camelKey = key.replace(/-(.)/, (_, firstLetter) => firstLetter.toUpperCase());
				if (camelKey in config) {
					any_keys = true;
					webhare[key] = config[camelKey];
				}
			}
		}
	}
	return any_keys ? webhare : null;
}

// Get a WebHare connection for a document, return a promise that resolves with a WebHareConnect when fully connected
export function getDocumentConnection(resource: string): Promise<WebHareConnection | null> {
	let result: Promise<WebHareConnection | null> | undefined = documentConnections.get(resource);
	if (!result) {
		result = getConnection(resource);
		documentConnections.set(resource, result);
	}
	return result;
}

// Remove the cached document connection
export function removeDocumentConnection(resource: string) {
	documentConnections.delete(resource);
}

export async function configurationChanged(config: { [key: string]: any }) {
	const webhare = getWebHareConfig(config);
	if (!webhare) {
		return;
	}
	for (const connPromise of webHareConnections.values()) {
		const conn = await connPromise;
		if (conn) {
			conn.sendNotification("didChangeConfiguration", webhare);
		}
	}
}
