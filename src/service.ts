/* Editor command handlers */

import {
	ApplyWorkspaceEditParams,
	CodeAction,
	CodeActionKind,
	CodeActionParams,
	Command,
	Definition,
	DidChangeTextDocumentParams,
	DidCloseTextDocumentParams,
	DidOpenTextDocumentParams,
	DidSaveTextDocumentParams,
	DocumentFormattingParams,
	ExecuteCommandParams,
	Hover,
	MessageType,
	PublishDiagnosticsParams,
	ShowDocumentParams,
	ShowMessageParams,
	TextDocumentPositionParams,
	TextEdit,
	WorkDoneProgressServerReporter,
	WorkspaceEdit
} from "vscode-languageserver";

import { ConfigParams, ConfigResponse, StackTraceParams, StackTraceResponse } from "./protocol";

import { connection, connectionConfig } from "./connection";

import { WebHareConnection, getDocumentConnection, removeDocumentConnection } from "./whconnect";


export async function didOpenTextDocumentNotification(params: DidOpenTextDocumentParams): Promise<void> {
	try {
		const whconnection = await getDocumentConnection(params.textDocument.uri);
		if (whconnection) {
			whconnection.sendNotification("didOpenTextDocumentNotification", whconnection.convertTextDocumentUri(params.textDocument));
		}
	} catch (e) {
		connection.console.error(`Error: ${e}`);
	}
}

export async function didChangeTextDocumentNotification(params: DidChangeTextDocumentParams): Promise<void> {
	try {
		const whconnection = await getDocumentConnection(params.textDocument.uri);
		if (whconnection)
		{
			whconnection.sendNotification("didChangeTextDocumentNotification", whconnection.convertTextDocumentUri(params.textDocument), params.contentChanges);
		}
	} catch (e) {
		connection.console.error(`Error: ${e}`);
	}
}

export async function didSaveTextDocumentNotification(params: DidSaveTextDocumentParams): Promise<void> {
	try {
		const whconnection = await getDocumentConnection(params.textDocument.uri);
		if (whconnection) {
			whconnection.sendNotification("didSaveTextDocumentNotification", whconnection.convertTextDocumentUri(params.textDocument), params.text);
		}
	} catch (e) {
		connection.console.error(`Error: ${e}`);
	}
}

export async function didCloseTextDocumentNotification(params: DidCloseTextDocumentParams): Promise<void> {
	try {
		const whconnection = await getDocumentConnection(params.textDocument.uri);
		if (whconnection) {
			whconnection.sendNotification("didCloseTextDocumentNotification", whconnection.convertTextDocumentUri(params.textDocument));
		}
		removeDocumentConnection(params.textDocument.uri);
	} catch (e) {
		connection.console.error(`Error: ${e}`);
	}
}

export async function definitionRequest(params: TextDocumentPositionParams): Promise<Definition> {
	try {
		const whconnection = await getDocumentConnection(params.textDocument.uri);
		if (!whconnection) {
			return [];
		}
		return whconnection.sendRequest("definitionRequest", whconnection.convertTextDocumentUri(params.textDocument), params.position);
	} catch (e) {
		connection.console.error(`Error: ${e}`);
		return Promise.reject(e);
	}
}

export async function hoverRequest(params: TextDocumentPositionParams): Promise<Hover | null> {
	try {
		const whconnection = await getDocumentConnection(params.textDocument.uri);
		if (!whconnection)
		{
			return null;
		}
		return whconnection.sendRequest("hoverRequest", whconnection.convertTextDocumentUri(params.textDocument), params.position);
	} catch (e) {
		connection.console.error(`Error: ${e}`);
		return Promise.reject(e);
	}
}

export async function formattingRequest(params: DocumentFormattingParams): Promise< TextEdit[] | null> {
	try {
		const whconnection = await getDocumentConnection(params.textDocument.uri);
		if (!whconnection) {
			return null;
		}
		const wd = await beginWorkDoneProgress("Formatting document...");
		const result = await whconnection.sendRequest("formattingRequest", whconnection.convertTextDocumentUri(params.textDocument), params.options);
		if (wd) {
			wd.done();
		}
		return result;
	} catch(e) {
		return Promise.reject();
	}
}

export async function codeActionRequest(params: CodeActionParams): Promise<(Command | CodeAction)[]> {
	try {
		const uri = params.textDocument.uri;
		const whconnection = await getDocumentConnection(uri);
		if (!whconnection) {
			return [];
		}
		let actions: CodeAction[] = await whconnection.sendRequest("codeActionRequest", whconnection.convertTextDocumentUri(params.textDocument), params.range, params.context);
		for (let action of actions) {
			if (!action.command) {
				continue;
			}
			if (!action.command.arguments) {
				action.command.arguments = [];
			}
			action.command.arguments.unshift(params.textDocument.uri);
		}
		if (connectionConfig.capabilities?.textDocument?.codeAction?.codeActionLiteralSupport?.codeActionKind.valueSet.includes(CodeActionKind.QuickFix)) {
			return actions;
		} else {
			let commands: Command[] = [];
			for (let action of actions)
			{
				if (action.command) {
					commands.push({
						title: action.title,
						command: action.command.command,
						arguments: action.command.arguments
					});
				}
			}
			return commands;
		}
	} catch (e) {
		connection.console.error(`Error: ${e}`);
		return Promise.reject(e);
	}
}

export async function executeCommandRequest(params: ExecuteCommandParams): Promise<WorkspaceEdit> {
	try {
		const result: WorkspaceEdit = {};
		if (params.arguments)
		{
			const uri = params.arguments[0];
			const whconnection = await getDocumentConnection(uri);
			if (whconnection) {
				let wd = null;
				switch (params.command) {
					case "organizeLoadlibs": {
						wd = await beginWorkDoneProgress("Organizing loadlibs...");
					}
				}
				params.arguments[0] = whconnection.getRelativePath(params.arguments[0]);
				const edits: TextEdit[] = await whconnection.sendRequest("executeCommandRequest", params.command, params.arguments);
				if (edits.length)
				{
					result.changes = {};
					result.changes[uri] = edits;
				}
				if (wd) {
					wd.done();
				}
			}
		}
		return result;
	} catch (e) {
		connection.console.error(`Error: ${e}`);
		return Promise.reject(e);
	}
}

// Handle a server request, may return a value that is sent back as a response to the server
export async function handleServerRequest(whconnection: WebHareConnection, method: string, params: any[]): Promise<any> {
	switch (method) {
		case "sendDiagnostics":
		{
			if (params.length > 0) {
				// Rewrite relative uri to absolute uri
				const diagnostics: PublishDiagnosticsParams = params[0];
				diagnostics.uri = whconnection.getAbsolutePath(diagnostics.uri);
				for (const diagnostic of diagnostics.diagnostics) {
					if (diagnostic.relatedInformation) {
						for (const related of diagnostic.relatedInformation) {
							related.location.uri = whconnection.getAbsolutePath(related.location.uri);
						}
					}
				}
				connection.sendDiagnostics(diagnostics);
			}
			return;
		}
		case "applyEdit":
		{
			if (params.length > 0) {
				const edits: ApplyWorkspaceEditParams = params[0];
				if (edits.edit.changes) {
					// Rewrite relative uris to absolute uris
					for (const uri of Object.keys(edits.edit.changes))
					{
						edits.edit.changes[whconnection.getAbsolutePath(uri)] = edits.edit.changes[uri];
						delete edits.edit.changes[uri];
					}
				}
				connection.workspace.applyEdit(edits);
			}
			return;
		}
		case "showMessage":
		{
			if (params.length > 0)
			{
				const param: ShowMessageParams = params[0];
				switch (param.type)
				{
					case MessageType.Error:
					{
						connection.window.showErrorMessage(param.message);
					} break;
					case MessageType.Warning:
					{
						connection.window.showWarningMessage(param.message);
					} break;
					case MessageType.Info:
					{
						connection.window.showInformationMessage(param.message);
					} break;
					case MessageType.Log:
					{
						connection.console.info(param.message);
					} break;
				}
			}
			return;
		}
		case "showDocument":
		{
			const param: ShowDocumentParams = params[0];
			// Rewrite relative uri to absolute uri
			connection.console.info("showDocument: "+param.uri);
			param.uri = whconnection.getAbsolutePath(param.uri);
			return await connection.window.showDocument(param);
		}
		case "workspaceFolders":
		{
			const folders = await connection.workspace.getWorkspaceFolders();
			if (folders) {
				for (const folder of folders) {
					folder.uri = whconnection.getRelativePath(folder.uri);
				}
			}
			return folders;
		}
	}
	throw new Error(`Unknown server request '${method}'`);
}

export async function stackTraceRequest(params: StackTraceParams): Promise<StackTraceResponse | null> {
	try {
		const whconnection = await getDocumentConnection(params.textDocument.uri);
		if (!whconnection) {
			return null;
		}
		return whconnection.sendRequest("stackTraceRequest", whconnection.convertTextDocumentUri(params.textDocument), params.lastGuid || "").then((result: StackTraceResponse) => {
			// Make the returned relative editor paths absolute again
			for (const error of result.errors) {
				for (const stack of error.stack) {
					stack.editorpath = whconnection.getAbsolutePath(stack.editorpath);
				}
			}
			return result;
		});
	} catch (e) {
		connection.console.error(`Error: ${e}`);
		return Promise.reject(e);
	}
}

export async function configRequest(params: ConfigParams): Promise<ConfigResponse | null> {
	try {
		const whconnection = await getDocumentConnection(params.textDocument.uri);
		if (!whconnection) {
			return null;
		}
		return await whconnection.getConfig();
	} catch (e) {
		connection.console.error(`Error: ${e}`);
		return Promise.reject(e);
	}
}

async function beginWorkDoneProgress(message: string): Promise<WorkDoneProgressServerReporter | null> {
	if (!connectionConfig.capabilities?.window?.workDoneProgress) {
		return null;
	}
	const wd = await connection.window.createWorkDoneProgress();
	wd.begin(message);
	return wd;
}
