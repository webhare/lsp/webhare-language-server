/* Shared code with editor connection */

import {
	ClientCapabilities,
	createConnection,
	ProposedFeatures
} from "vscode-languageserver/node";

// Create a connection for the server. The connection uses stdin/stdout as a transport.
export const connection = createConnection(ProposedFeatures.all);

interface ConnectionConfig {
	capabilities: ClientCapabilities | null;
	initializationOptions?: any;
}

// Shared connection configuration
export const connectionConfig: ConnectionConfig = {
	capabilities: null,
	initializationOptions: null
};
