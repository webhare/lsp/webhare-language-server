/* The main script that handles the editor connection */

import {
	CodeActionKind,
	DidChangeConfigurationNotification,
	DidChangeConfigurationParams,
	InitializeParams,
	TextDocumentSyncKind
} from "vscode-languageserver/node";

import { connection, connectionConfig } from "./connection";
import { ConfigRequest, StackTraceRequest } from "./protocol";

import {
	codeActionRequest,
    configRequest,
	definitionRequest,
	didChangeTextDocumentNotification,
	didCloseTextDocumentNotification,
	didOpenTextDocumentNotification,
	didSaveTextDocumentNotification,
	executeCommandRequest,
	formattingRequest,
	hoverRequest,
	stackTraceRequest
} from "./service";

import { configurationChanged } from "./whconnect";


// Export the protocol exports to users of the library
export * from "./protocol";

// Read the package version
const { version } = require("../package.json");

let clientName: string | undefined;

// Initialize the editor connection
connection.onInitialize((params: InitializeParams) => {
	// Store the connection configuration
	connectionConfig.capabilities = params.capabilities;
	connectionConfig.initializationOptions = params.initializationOptions;
	clientName = params.clientInfo?.name;

	// Return our capabilities
	return {
		capabilities: {
			textDocumentSync: {
				openClose: true,
				change: TextDocumentSyncKind.Incremental,
				save: { includeText: true }
			},
			definitionProvider: true,
			documentFormattingProvider: true,
			hoverProvider: true,
			codeActionProvider: {
				codeActionKinds: [
					CodeActionKind.QuickFix,
					CodeActionKind.Source,
					CodeActionKind.SourceOrganizeImports
				]
			},
			executeCommandProvider: {
				commands: [
					"addMissingLoadlib",
					"removeUnusedLoadlib",
					"organizeLoadlibs"
				]
			}
		},
		serverInfo: {
			name: "WebHare",
			version
		}
	};
});

connection.onInitialized(() => {
	if (connectionConfig.capabilities?.workspace?.configuration) {
		// Needed for VSCode, but breaks Nova :-(
		if (clientName != "Nova") {
			connection.client.register(DidChangeConfigurationNotification.type, undefined);
		}
		connection.onDidChangeConfiguration((params: DidChangeConfigurationParams) => {
			// VSCode doesn't send the updated settings in the params, so we'll just fetch the current settings
			if (!params.settings) {
				connection.workspace.getConfiguration("webhare").then(settings => configurationChanged(settings));
			} else if ("webhare" in params.settings) {
				configurationChanged(params.settings.webhare);
			}
		});
	}
});


connection.onDidOpenTextDocument(didOpenTextDocumentNotification);
connection.onDidChangeTextDocument(didChangeTextDocumentNotification);
connection.onDidSaveTextDocument(didSaveTextDocumentNotification);
connection.onDidCloseTextDocument(didCloseTextDocumentNotification);

connection.onDefinition(definitionRequest);

connection.onHover(hoverRequest);

connection.onDocumentFormatting(formattingRequest);

connection.onCodeAction(codeActionRequest);

connection.onExecuteCommand(executeCommandRequest);

connection.onRequest(StackTraceRequest.type, stackTraceRequest);
connection.onRequest(ConfigRequest.type, configRequest);

connection.listen();
